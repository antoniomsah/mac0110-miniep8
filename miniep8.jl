function value(x)
  if x == 'A'
    return 'E'
  end
  if x == 'K'
    return 'D'
  end
  if x == 'Q'
    return 'C'
  end
  if x == 'J'
    return 'B'
  end
  if x == '1'
    return 'A'
  else
    return x
  end
end

function suitvalue(x)
  if x == '♦'
    return 'A'
  end
  if x == '♠'
    return 'B'
  end
  if x == '♥'
    return 'C'
  end
  if x == '♣'
    return 'D'
  else
    return 0
  end
end

function compareByValueAndSuit(x, y)
  if x[2] == '0'
    naipeX = x[3]
  else
    naipeX = x[2]
  end
  if y[2] == '0'
    naipeY = y[3]
  else
    naipeY = y[2]
  end
  if suitvalue(naipeX) < suitvalue(naipeY)
    return true
  end
  if suitvalue(naipeX) > suitvalue(naipeY)
    return false
  else
    X = value(x[1])
    Y = value(y[1])
    if X < Y
      return true
    else
      return false
    end
  end
end

function troca(v, i, j)
  aux = v[i]
  v[i] = v[j]
  v[j] = aux
end

function insercao(v)
  tam = length(v)
  for i in 2:tam
    j = i
    while j > 1
      if compareByValueAndSuit(v[j], v[j - 1]) == true
        troca(v, j, j - 1)
      else
        break
      end
      j = j - 1
    end
  end
  return v
end
